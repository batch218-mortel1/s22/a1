/*
    Create functions which can manipulate our arrays.
*/

// console.log("Testing")

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];


/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/

function registerNewName(addName){
    
    let theExistingName = registeredUsers.includes("James Jeffries");
    alert("Registration failed. Username already exists!");

    if (theExistingName != -1){
    
        addName.push("Bella Porch")
        alert("Thank you for registering!")
    }
    
}

console.log(registeredUsers);

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

function addBuddy(theExistingName){
    
    let friendsList = registeredUsers.includes("Apo Calypto");
    alert("User not found")

    if (friendsList = 6){
        theExistingName.push("Bella Porch")

    }
}

console.log(friendsList);


/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

function showFriendsList(friendsList) {
    
    if (friendsList.length == 0) {
        alert("You currently have 0 friends. Add one first.");
        
    } else {
        friendsList.forEach(value => {
            console.log(value);
        });
    }
}

console.log("Displaying list of friends.")
showFriendsList(friendsList); 



/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/


function showNumberOfFriends(friendsList) {
    
    if (friendsList.length == 0) {
        alert("You currently have 0 friends. Add one first.");
    } else  {

        friendList.length == 1 
            alert("You currently have " + friendsList.length + " friends.")
        
        
    }
}

showNumberOfFriends(friendsList);



/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/


function eraseLastAddedNameFromFriendsList() {
    
    if (friendsList.length == 0 ) {
        alert("You currently have no friends. Add one first.");
      
        friendsList.pop();
    }
}

eraseLastAddedNameFromFriendsList();
console.log("Show friends list");
console.log(friendsList);
eraseLastAddedNameFromFriendsList();

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/



function deleteFriend(randomFriend) {
    
    let friendsIndex = friendsList.indexOf(randomFriend);
    let isAFriend = friendsIndex != -1;

    if (friendsList.length == 0 ) {
        alert("You currently have no friends. Add one first.");
        
    } else if (isAFriend) {
        friendsList.splice(friendsIndex, 1);
    } else {
        alert("User " + randomFriend + " is registered but not a friend");
        
    }

}


console.log("To delete user Macie West from friendsList");
deleteFriend("Macie West");

friendsList = registeredUsers;
console.log(friendsList);

console.log("Macie West was deleted from friendsList");
deleteFriend("Macie West");

console.log("Show friendsList");
console.log(friendsList);
